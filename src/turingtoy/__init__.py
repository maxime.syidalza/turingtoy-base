from typing import (
    Dict,
    List,
    Optional,
    Tuple,
    Literal,
    Union,
    TypedDict,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


Movement = Literal["L", "R"]
Action = Literal["write", Movement]
Instruction = Union[Movement, Dict[Action, str]]
TransitionTable = Dict[str, Dict[str, Instruction]]
TuringMachine = TypedDict(
    "TuringMachine",
    {
        "blank": str,
        "start state": str,
        "final states": List[str],
        "table": TransitionTable,
    },
)


class Entry(TypedDict):
    state: str
    reading: str
    position: int
    tape: str
    transition: Instruction


def run_turing_machine(
    machine: TuringMachine,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List[Entry], bool]:
    transition_table = machine["table"]
    final_states = machine["final states"]
    blank_symbol = machine["blank"]

    # Initialize tape with input
    tape: List[str] = list(input_)
    history: List[Entry] = []
    state = machine["start state"]
    position = 0
    steps_taken = 0

    while state not in final_states:
        if steps is not None and steps_taken >= steps:
            break

        if position < 0:
            tape.insert(0, blank_symbol)
            position = 0
        elif position >= len(tape):
            tape.append(blank_symbol)

        symbol = tape[position]

        if state not in transition_table or symbol not in transition_table[state]:
            break

        instruction = transition_table[state][symbol]

        history_entry: Entry = {
            "state": state,
            "reading": symbol,
            "position": position,
            "tape": "".join(tape),
            "transition": instruction,
        }
        history.append(history_entry)

        if "write" in instruction:
            tape[position] = instruction["write"]

        if "L" in instruction:
            position -= 1
        elif "R" in instruction:
            position += 1

        if isinstance(instruction, str):
            state = instruction
        elif isinstance(instruction, dict):
            state = instruction.get("L", instruction.get("R", state))

        steps_taken += 1

    final_tape = "".join(tape).strip(blank_symbol)
    halted = state in final_states

    return final_tape, history, halted